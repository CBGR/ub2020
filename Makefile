# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Aggregate and process input data and metadata, producing output archives for
# deployment.

BASEDIR = $(CURDIR)
TOOLS = $(BASEDIR)/tools
DATA = $(BASEDIR)/data
DATATOOLS = $(TOOLS)/data
SQLTOOLS = $(TOOLS)/sqlite



# Output data.

OUTPUT_DATA		?= /var/tmp/unibind-data

# Input data and metadata.

ifndef INPUT_DATA
$(error INPUT_DATA must be defined)
endif

# Obtain the most recent metadata file.

INPUT_METADATA		?= $(shell ls -1t $(INPUT_DATA)/metadata* | head -n 1)

# Enrichment data is stored in separate collections.

COLLECTIONS		?= $(shell $(DATATOOLS)/get_collections $(INPUT_DATA))



# Metadata processing artefacts and archive inputs.

WORK			?= $(BASEDIR)/work
DATABASE		= $(WORK)/experiments.sqlite
AGGREGATED		= $(WORK)/aggregated
BULKDATA		= $(WORK)/bulk
BULKDATA_ARCHIVED	= $(WORK)/bulk_archived
METADATA		= $(WORK)/metadata
TFBSDATA		= $(WORK)/tfbs

# Additional metadata.

EXTRA_METADATA		= $(DATA)/unibind_examples $(DATA)/unibind_info
RENAMED_FILES		= $(METADATA)/renamed_files
RENAMED_COLLECTION_FILES= $(METADATA)/renamed_collection_files

# Completion indicators.

PREPARED_METADATA	= $(AGGREGATED)/completed
EXPORTED_METADATA	= $(METADATA)/completed
RESTRUCTURED_BULK_DATA	= $(WORK)/completed_bulk
RESTRUCTURED_DATA	= $(WORK)/completed

# Eventual archive filenames.

ARCHIVE_DATA		= $(shell $(DATATOOLS)/archive_filename $(INPUT_DATA) '' 'tfbs' $(WORK))
ARCHIVE_METADATA	= $(shell $(DATATOOLS)/archive_filename $(INPUT_DATA) '' 'tfbs_metadata' $(WORK))

# Various kinds of data for each collection are archived separately.

ARCHIVE_BULK_DATA	= $(foreach COLLECTION,$(COLLECTIONS),\
			  $(shell $(DATATOOLS)/archive_filename $(INPUT_DATA) $(COLLECTION) bulk $(WORK) --uncompressed))

ARCHIVE_ENRICHMENT_DATA	= $(foreach COLLECTION,$(COLLECTIONS),\
			  $(shell $(DATATOOLS)/archive_filename $(INPUT_DATA) $(COLLECTION) LOLA_databases $(WORK)))

ARCHIVE_TRACKS_DATA	= $(foreach COLLECTION,$(COLLECTIONS),\
			  $(shell $(DATATOOLS)/archive_filename $(INPUT_DATA) $(COLLECTION) UniBind_hubs $(WORK)))

# Summary of outputs.

OUTPUTS			= $(ARCHIVE_DATA) $(ARCHIVE_METADATA) \
			  $(ARCHIVE_BULK_DATA) $(ARCHIVE_ENRICHMENT_DATA) \
			  $(ARCHIVE_TRACKS_DATA)

# Determine all outputs for deletion purposes.

ALL_OUTPUTS		= $(OUTPUTS) $(RESTRUCTURED_DATA) $(DATABASE) \
			  $(wildcard $(METADATA)/*)



# Processing workflow.

.PHONY : all clean install

all: $(OUTPUTS)

clean:
	$(RM) $(ALL_OUTPUTS)
	$(RM) -r $(BULKDATA)
	$(RM) -r $(BULKDATA_ARCHIVED)
	$(RM) -r $(TFBSDATA)
	$(MAKE) -f $(DATATOOLS)/prepare_metadata.mk clean INPUT_DATA=$(INPUT_DATA) INPUT_METADATA=$(INPUT_METADATA) WORK=$(WORK)
	-rmdir $(METADATA)
	-rmdir $(WORK)

install: $(OUTPUTS) $(OUTPUT_DATA)
	cp $(OUTPUTS) $(OUTPUT_DATA)



# Make the output directory.

$(OUTPUT_DATA):
	mkdir -p $(OUTPUT_DATA)



# Archive production using the restructured data and exported (processed)
# metadata.

$(ARCHIVE_DATA): $(RESTRUCTURED_DATA)
	$(TOOLS)/tar_create $(ARCHIVE_DATA) $(TFBSDATA) --follow-links

$(ARCHIVE_METADATA): $(EXPORTED_METADATA)
	$(TOOLS)/tar_create $(ARCHIVE_METADATA) $(METADATA)



# Prepare metadata using the input data and metadata.

$(PREPARED_METADATA): $(INPUT_DATA) $(INPUT_METADATA)
	$(MAKE) -f $(DATATOOLS)/prepare_metadata.mk INPUT_DATA=$(INPUT_DATA) INPUT_METADATA=$(INPUT_METADATA) WORK=$(WORK)



# Prepare a database containing the metadata.

$(DATABASE): $(PREPARED_METADATA)
	$(SQLTOOLS)/prepare_metadata $(AGGREGATED) $(DATABASE)

# Export the metadata records and record processing completion.
# Include extra metadata here.

$(EXPORTED_METADATA): $(DATABASE) $(EXTRA_METADATA)
	mkdir -p $(METADATA)
	$(SQLTOOLS)/export_unibind_records $(METADATA) $(DATABASE)

	# Copy extra metadata and modify it appropriately.

	cp $(EXTRA_METADATA) $(METADATA)
	$(DATATOOLS)/tag_info $(INPUT_DATA) $(METADATA)/unibind_info

	touch $(EXPORTED_METADATA)



# Data restructuring operates on the input data and uses the renamed file
# information. Completion is then indicated using a special file.

$(RESTRUCTURED_BULK_DATA): $(INPUT_DATA) $(RENAMED_COLLECTION_FILES)
	$(DATATOOLS)/restructure_bulk_data $(INPUT_DATA) $(BULKDATA) < $(RENAMED_COLLECTION_FILES)
	$(DATATOOLS)/copy_bulk_data $(INPUT_DATA) $(BULKDATA)
	$(DATATOOLS)/archive_bulk_data $(BULKDATA) $(BULKDATA_ARCHIVED)
	touch $(RESTRUCTURED_BULK_DATA)

$(RESTRUCTURED_DATA): $(INPUT_DATA) $(RENAMED_FILES)
	$(DATATOOLS)/restructure_data $(INPUT_DATA) $(TFBSDATA) < $(RENAMED_FILES)
	touch $(RESTRUCTURED_DATA)

# Renamed file details are part of the exported metadata.

$(RENAMED_FILES): $(EXPORTED_METADATA)

# Renamed files by collection.

$(RENAMED_COLLECTION_FILES): $(RENAMED_FILES) $(PREPARED_METADATA)
	$(DATATOOLS)/get_renamed_folder_membership $(AGGREGATED)/folder_membership $(RENAMED_FILES) > $(RENAMED_COLLECTION_FILES)



# Create rules for each collection.

include $(DATATOOLS)/archive_bulk.mk
include $(DATATOOLS)/archive_lola.mk
include $(DATATOOLS)/archive_tracks.mk

# Expand the templates for each collection.

$(foreach COLLECTION,$(COLLECTIONS),\
$(eval $(call bulk_collection_template,$(COLLECTION))))

$(foreach COLLECTION,$(COLLECTIONS),\
$(eval $(call lola_collection_template,$(COLLECTION))))

$(foreach COLLECTION,$(COLLECTIONS),\
$(eval $(call tracks_collection_template,$(COLLECTION))))
