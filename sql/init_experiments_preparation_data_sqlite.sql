-- SPDX-FileCopyrightText: 2020 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- Taxonomy names.

create index taxonomy_names_index on taxonomy_names(scientific_name);
analyze taxonomy_names;

-- Matrix scores.

create index matrix_scores_index on matrix_scores(folder);
analyze matrix_scores;

-- Peak records.

create index peak_totals_index on peak_totals(folder);
analyze peak_totals;

-- TFBS records.

create index tfbs_totals_index on tfbs_totals(folder);
analyze tfbs_totals;

-- Experiment profiles.

create index experiment_profiles_index on experiment_profiles(UBID);
analyze experiment_profiles;

-- Folders.

create index folders_index on folders(UBID);
analyze folders;

-- Folder membership.

create index folder_membership_index on folder_membership(UBID);
analyze folder_membership;



-- Replace or fix various null values in the experiments table.

update experiments set cell_id = 0 where cell_id = 'NA';
update experiments set external_db = 'AE' where external_db = 'NA' and external_db_id like 'ERR%';
update experiments set external_db = 'AE' where external_db = 'Array Express';
update experiments set external_db = 'ENCODE' where external_db = 'ENCODEPROJ';
update experiments set treatment = null where treatment in ('', 'NA', 'NULL');
update experiments set exp_factor_ontology_id = null where exp_factor_ontology_id in ('', 'NA', 'NULL');

-- Some species may need to be corrected.
-- This needs to be done using two operations because SQLite seems to think that
-- the species will end up being null in the update statement unless a where
-- condition is used that duplicates the species column query.

create temporary table tmp_new_species as
    select E.UBID, folders.species
    from experiments as E
    inner join folders
        on E.UBID = folders.UBID
    where E.species <> folders.species;

update experiments set species = (
    select species
    from tmp_new_species as E
    where E.UBID = experiments.UBID)
    where UBID in (
        select UBID from tmp_new_species);

-- NOTE: Sometimes, cell type and experiment identifiers are missing from the
-- NOTE: metadata but are encoded in the UBID. Here, this information is
-- NOTE: recovered.

create temporary table tmp_ubid_details as
    select UBID, substr(UBID, 1, instr(UBID, '_') - 1) as identifier,
                 substr(UBID, instr(UBID, '_') + 1) as remainder
    from folders;

create temporary table tmp_recovered_ids as
    select UBID,
           identifier as recovered_identifier,
           substr(remainder, 1, instr(remainder, '_') - 1) as recovered_cell_type,
           substr(remainder, instr(remainder, '_') + 1) as recovered_tf_name
    from tmp_ubid_details
    where UBID like 'ENC%'
    union all
    select UBID,
           identifier as recovered_identifier,
           substr(remainder, instr(remainder, '_') + 1) as recovered_cell_type,
           substr(remainder, 1, instr(remainder, '_') - 1) as recovered_tf_name
    from tmp_ubid_details
    where UBID not like 'ENC%';

-- Choose folder identifiers from either external/original database identifiers
-- and GTRD identifiers. Here, the single identifier corresponding to each
-- folder's records is chosen.

delete from chosen_folder_ids;

insert into chosen_folder_ids
    select min(external_db_id) as identifier, UBID
    from experiments
    where external_db_id <> 'NA'
    group by UBID
    having count(distinct external_db_id) = 1;

insert into chosen_folder_ids
    select min(id) as identifier, UBID
    from experiments
    where external_db_id <> 'NA'
    group by UBID
    having count(distinct external_db_id) > 1 and count(distinct id) = 1;

-- NOTE: Sometimes, identifiers are completely absent from the metadata but are
-- NOTE: encoded in the UBID.

insert into chosen_folder_ids
    select recovered_identifier as identifier, experiments.UBID
    from experiments
    inner join tmp_recovered_ids
        on experiments.UBID = tmp_recovered_ids.UBID
    where external_db_id = 'NA';

-- Augment the folder details with the chosen identifiers.
-- NOTE: Sometimes, cell types are absent from the metadata but are encoded in
-- NOTE: the UBID. Measures to restore them are introduced below.

create temporary table tmp_folder_ids as
    select identifier, folders.UBID, tf_name, species,
           case cell_type_rewritten when 'NA' then recovered_cell_type else cell_type_rewritten end as cell_type_rewritten,
           tf_name_rewritten,
           treatment_rewritten,
           recovered_cell_type
    from chosen_folder_ids
    inner join folders
        on chosen_folder_ids.UBID = folders.UBID
    inner join tmp_recovered_ids
        on chosen_folder_ids.UBID = tmp_recovered_ids.UBID;

-- Formulate folder names based on the metadata in long and short forms.
-- Long forms include the treatment value if appropriate.

create temporary table tmp_folder_alternatives as
    select identifier || '.' || cell_type_rewritten || '_' || treatment_rewritten || '.' || tf_name_rewritten as folder_long,
           identifier || '.' || cell_type_rewritten || '.' || tf_name_rewritten as folder_short,
           identifier || '.' || recovered_cell_type || '.' || tf_name_rewritten as folder_recovered,
           UBID,
           tf_name,
           species,
           tf_name_rewritten
    from tmp_folder_ids
    where treatment_rewritten not in ('NA', 'NULL')
    union all
    select identifier || '.' || cell_type_rewritten || '.' || tf_name_rewritten as folder_long,
           identifier || '.' || cell_type_rewritten || '.' || tf_name_rewritten as folder_short,
           identifier || '.' || recovered_cell_type || '.' || tf_name_rewritten as folder_recovered,
           UBID,
           tf_name,
           species,
           tf_name_rewritten
    from tmp_folder_ids
    where treatment_rewritten in ('NA', 'NULL');

-- Create a table to obtain the most concise folder name.

create temporary table tmp_folders as
    select folder_short as folder, UBID, tf_name, species, tf_name_rewritten
    from tmp_folder_alternatives
    where folder_short in (
        select folder_short
        from tmp_folder_alternatives
        group by folder_short
        having count(*) = 1)
    union all
    select folder_long as folder, UBID, tf_name, species, tf_name_rewritten
    from tmp_folder_alternatives
    where folder_short in (
        select folder_short
        from tmp_folder_alternatives
        group by folder_short
        having count(*) > 1)
        and folder_long not in (
        select folder_long
        from tmp_folder_alternatives
        group by folder_long
        having count(*) > 1)
    union all
    select folder_recovered as folder, UBID, tf_name, species, tf_name_rewritten
    from tmp_folder_alternatives
    where folder_short in (
        select folder_short
        from tmp_folder_alternatives
        group by folder_short
        having count(*) > 1)
        and folder_long in (
        select folder_long
        from tmp_folder_alternatives
        group by folder_long
        having count(*) > 1);

-- Create a convenience table for factors providing enough information to be
-- preserved in the exported factors table.

delete from renamed_folders;

insert into renamed_folders
    select tmp_folders.folder, UBID, tmp_folders.tf_name, tmp_folders.species, total_peaks, tax_id, tf_name_rewritten
    from tmp_folders

    -- Obtain peak total information.

    inner join peak_totals
        on tmp_folders.UBID = peak_totals.folder

    -- Obtain species information.

    inner join taxonomy_names
        on species = scientific_name;



-- Make records for import.
-- Note also that Django's ORM will put columns in its own order, and so the
-- ordering in these tables has to correspond to that.

-- Prediction model or computation records.

delete from unibind_factor_data;

insert into unibind_factor_data
    select distinct
           'MACS'                               as peak_caller,
           'DAMO'                               as prediction_model,
           null                                 as model_detail,
           enrichment_zone_threshold            as distance_threshold,
           score_threshold                      as score_threshold,
           adjusted_centrality_pvalue           as adj_centrimo_pvalue,
           total_tfbs                           as total_tfbs,
           experiment_profiles.jaspar_id        as jaspar_id,
           experiment_profiles.jaspar_version   as jaspar_version,
           renamed_folders.folder               as folder,
                                                -- must be last due to Django/SQLite limitation
           null                                 as profile_id
                                                -- must be last due to Django/SQLite limitation
    from experiments

    -- Obtain a profile.

    inner join experiment_profiles
        on experiments.UBID = experiment_profiles.UBID

    -- Obtain score information.

    inner join matrix_scores
        on experiments.UBID = matrix_scores.folder
        and experiment_profiles.jaspar_id = matrix_scores.jaspar_id
        and experiment_profiles.jaspar_version = matrix_scores.jaspar_version

    -- Obtain TFBS information.

    inner join tfbs_totals
        on experiments.UBID = tfbs_totals.folder
        and experiment_profiles.jaspar_id = tfbs_totals.jaspar_id
        and experiment_profiles.jaspar_version = tfbs_totals.jaspar_version

    -- Impose constraints applying to the folder/factor since dataset records
    -- depend on the preservation of the folder/factor, but the folder/factor
    -- records depend on the preservation of datasets, and we have to generate
    -- one of these things first.

    inner join renamed_folders
        on experiments.UBID = renamed_folders.UBID;

-- Folder/factor records. Note that the surrogate key is missing since it will
-- be added in the UniBind database for any records that are new.

delete from unibind_factor;

insert into unibind_factor
    select distinct tf_name, renamed_folders.folder, total_peaks, tax_id
    from renamed_folders

    -- Join to the generated dataset records, thus eliminating factors/folders
    -- without usable data.

    inner join unibind_factor_data
        on renamed_folders.folder = unibind_factor_data.folder;

-- Sample records.

delete from unibind_factor_sample;

insert into unibind_factor_sample
    select distinct
           treatment                            as biological_condition,
           cell_id                              as cell_id,
           unibind_factor.folder                as folder
    from experiments

    -- Obtain the renamed folder.

    inner join renamed_folders
        on experiments.UBID = renamed_folders.UBID

    -- Join to the generated records.

    inner join unibind_factor
        on renamed_folders.folder = unibind_factor.folder;

-- Database references. Note that the surrogate key is missing since it will be
-- added in the UniBind database for any records that are new.

delete from unibind_factor_ref;

insert into unibind_factor_ref
    select distinct
           external_db_id as ref_id,
           unibind_factor.folder as folder,
           external_db as ref_db
    from experiments

    -- Obtain the renamed folder.

    inner join renamed_folders
        on experiments.UBID = renamed_folders.UBID

    -- Join to the generated records.

    inner join unibind_factor
        on renamed_folders.folder = unibind_factor.folder

    where external_db_id <> 'NA'

    union all

    -- Obtain GTRD identifiers if any are assigned.

    select distinct
           id as ref_id,
           unibind_factor.folder as folder,
           'GTRD' as ref_db
    from experiments

    -- Obtain the renamed folder.

    inner join renamed_folders
        on experiments.UBID = renamed_folders.UBID

    -- Join to the generated records.

    inner join unibind_factor
        on renamed_folders.folder = unibind_factor.folder

    where id <> 'NA'

    union all

    -- Obtain recovered identifiers if necessary.

    select distinct
           identifier as ref_id,
           unibind_factor.folder as folder,
           external_db as ref_db
    from experiments

    -- Obtain the recovered identifier.

    inner join chosen_folder_ids
        on experiments.UBID = chosen_folder_ids.UBID

    -- Obtain the renamed folder.

    inner join renamed_folders
        on experiments.UBID = renamed_folders.UBID

    -- Join to the generated records.

    inner join unibind_factor
        on renamed_folders.folder = unibind_factor.folder

    where external_db_id = 'NA' and id = 'NA';

-- Cell details.

create temporary table tmp_longest_titles as
    select experiments.cell_id as cell_id,
           min(title) as longest_title
    from experiments inner join (
        select cell_id, max(length(title)) as longest
        from experiments
        group by cell_id
        ) as lengths
        on experiments.cell_id = lengths.cell_id and length(title) = longest
    group by experiments.cell_id, longest;

delete from unibind_cell;

insert into unibind_cell
    select experiments.cell_id,
           min(longest_title),
           min(cellosaurus_id),
           min(cell_ontology_id),
           min(exp_factor_ontology_id),
           min(uberon_id),
           min(source),
           min(source_id),
           min(cell_type_id),
           min(brenda_id),
           min(tax_id)              -- obtained using species name from taxonomy information
                                    -- must be last due to Django/SQLite limitation
    from experiments
    inner join taxonomy_names
        on species = scientific_name

    -- Join to the generated records.

    inner join unibind_factor_sample
        on experiments.cell_id = unibind_factor_sample.cell_id

    -- Obtain the most descriptive titles.

    inner join tmp_longest_titles
        on experiments.cell_id = tmp_longest_titles.cell_id

    -- Exclude undefined cell types.

    where experiments.cell_id <> 0

    -- Group by the principal identifier, with the other columns hopefully
    -- producing duplicate values, potentially mixed in with nulls.

    group by experiments.cell_id;

-- Collection membership.

delete from unibind_factor_collection;

insert into unibind_factor_collection
    select collection, unibind_factor.folder
    from folder_membership

    -- Obtain the renamed folder.

    inner join renamed_folders
        on folder_membership.UBID = renamed_folders.UBID

    -- Join to the generated records.

    inner join unibind_factor
        on renamed_folders.folder = unibind_factor.folder;

delete from unibind_species_collection;

insert into unibind_species_collection
    select collection, tax_id
    from unibind_factor_collection
    inner join unibind_factor
        on unibind_factor_collection.folder = unibind_factor.folder
    group by collection, tax_id
    order by collection, tax_id;

delete from unibind_collection;

insert into unibind_collection
    select distinct collection
    from unibind_species_collection
    order by collection;



-- Create a convenience table for renaming files.

delete from renamed_files;

insert into renamed_files
    select renamed_folders.folder, UBID, tf_name, species, jaspar_id, jaspar_version, tf_name_rewritten
    from renamed_folders
    inner join unibind_factor_data
        on renamed_folders.folder = unibind_factor_data.folder;
