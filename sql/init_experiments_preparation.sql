-- SPDX-FileCopyrightText: 2020 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

-- UniBind experiment-related data originating from GTRD.

-- It seems as if the specie and species columns always contain the same value,
-- and perhaps this is a kind of join column in two different tables.

-- Practically everything can apparently be 'NA', which should correspond to
-- null in exported data. Many fields can also be 'NULL' which should be
-- interpreted as null.

create table experiments (

    -- References.

    external_db_id varchar not null,
    cell_id varchar not null,               -- GTRD cell accession (see below), can be 'NA'
    id varchar not null,                    -- GTRD chip experiment accession (see below)
    external_db varchar not null,

    -- Chip experiment information.

    antibody varchar not null,
    specie varchar not null,                -- superfluous (see species)
    treatment varchar,
    control_id varchar,
    experiment_type varchar not null,       -- can be 'unspecified'
    tf_uniprot_id varchar,

    -- Cell information.

    title varchar,
    cellosaurus_id varchar,
    species varchar not null,               -- matches "specie" above
    cell_ontology_id varchar,
    exp_factor_ontology_id varchar,
    uberon_id varchar,
    source varchar,
    source_id varchar,
    cell_type_id varchar,
    brenda_id varchar,

    -- UniBind details.

    UBID varchar not null,
    source_db varchar not null
);

-- Organism details.

create table taxonomy_names (
    tax_id integer not null,
    scientific_name varchar not null
);

-- Score information.

create table matrix_scores (
    folder varchar not null,
    jaspar_id varchar not null,
    jaspar_version integer not null,
    score_threshold varchar not null,
    enrichment_zone_threshold varchar not null,
    enrichment_zone_width varchar not null,
    adjusted_centrality_pvalue varchar not null
);

-- Peak details.

create table peak_totals (
    folder varchar not null,
    total_peaks integer not null
);

-- Peak details.

create table tfbs_totals (
    folder varchar not null,
    jaspar_id varchar not null,
    jaspar_version varchar not null,
    total_tfbs integer not null
);

-- Mapping to profiles from experiments.

create table experiment_profiles (
    UBID varchar not null,
    jaspar_id varchar not null,
    jaspar_version varchar not null
);

-- Dataset folder details.

create table folders (
    UBID varchar not null,                  -- original folder name
    tf_name varchar not null,
    species varchar not null,               -- species name

    -- Rewritten values for folder renaming.

    cell_type_rewritten varchar not null,
    tf_name_rewritten varchar not null,
    treatment_rewritten varchar not null
);

-- Dataset membership details.

create table folder_membership (
    collection varchar not null,
    UBID varchar not null                   -- original folder name
);

-- Mappings from chosen identifiers to folders.

create table chosen_folder_ids (
    identifier varchar not null,
    UBID varchar not null
);

-- Renamed folder details.

create table renamed_folders (
    folder varchar not null,
    UBID varchar not null,                  -- original folder name
    tf_name varchar not null,
    species varchar not null,               -- species name
    total_peaks integer not null,
    tax_id integer not null,
    tf_name_rewritten varchar not null
);

-- Renamed file details.

create table renamed_files (
    folder varchar not null,
    UBID varchar not null,                  -- original folder name
    tf_name varchar not null,
    species varchar not null,               -- species name
    jaspar_id varchar not null,
    jaspar_version varchar not null,
    tf_name_rewritten varchar not null
);



-- Records for import.

-- Folder table corresponding to factor in UniBind.

create table unibind_factor (
    tf_name varchar,
    folder varchar,                 -- natural key
    total_peaks integer,
    tax_id integer
);

-- Sample data corresponding to factor_sample in UniBind.

create table unibind_factor_sample (
    biological_condition varchar,
    cell_id integer,                -- references unibind_cell
    folder varchar                  -- references unibind_factor
);

-- Folder data corresponding to factor_data in UniBind.

create table unibind_factor_data (
    peak_caller varchar,
    prediction_model varchar,
    model_detail varchar,
    distance_threshold varchar,
    score_threshold varchar,
    adj_centrimo_pvalue varchar,
    total_tfbs varchar,
    jaspar_id varchar,
    jaspar_version varchar,
    folder varchar,                 -- references unibind_factor
    profile_id integer
);

-- Reference table corresponding to factor_ref in UniBind.

create table unibind_factor_ref (
    ref_id varchar not null,
    folder varchar not null,        -- references unibind_factor
    ref_db varchar not null
);

-- Cell table corresponding to cell in UniBind.

create table unibind_cell (
    cell_id integer not null,
    title varchar not null,
    cellosaurus_id varchar,
    cell_ontology_id varchar,
    exp_factor_ontology_id varchar,
    uberon_id varchar,
    source varchar,
    source_id varchar,
    cell_type_id varchar,
    brenda_id varchar,
    tax_id integer not null
);

-- Folder membership corresponding to factor_collection in UniBind.
-- The column order here reflects Django-plus-SQLite's absurd limitations.

create table unibind_factor_collection (
    collection varchar not null,
    folder varchar not null         -- references unibind_factor
);

-- Species membership corresponding to species_collection in UniBind.
-- The column order here reflects Django-plus-SQLite's absurd limitations.

create table unibind_species_collection (
    collection varchar not null,
    tax_id integer not null
);

-- Collection enumeration.

create table unibind_collection (
    collection varchar not null
);
