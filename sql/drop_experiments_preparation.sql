-- SPDX-FileCopyrightText: 2020 University of Oslo
-- SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
-- SPDX-License-Identifier: GPL-3.0-or-later

drop table experiments;
drop table taxonomy_names;
drop table matrix_scores;
drop table peak_totals;
drop table tfbs_totals;
drop table experiment_profiles;
drop table folders;
drop table folder_membership;
drop table chosen_folder_ids;
drop table renamed_folders;
drop table renamed_files;
drop table unibind_cell;
drop table unibind_collection;
drop table unibind_factor;
drop table unibind_factor_collection;
drop table unibind_factor_data;
drop table unibind_factor_ref;
drop table unibind_factor_sample;
drop table unibind_species_collection;
