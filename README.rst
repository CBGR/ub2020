UniBind Data Processing
=======================

This distribution provides tools to consume data processed for a UniBind
release, preparing metadata and structuring data for publication via the
UniBind Web interface.

As input, a directory structure containing species dataset directories and
some additional files needs to be present. The directories are currently
these:

========================= ===================================================
Directory                 Contents
========================= ===================================================
``DAMO``                  Per-species dataset directories
``Permissive_collection`` Per-species records, bulk, enrichment, track data
``Robust_collection``     (As for other collections)
========================= ===================================================

The species directories are currently these for the permissive collection:

::

  Arabidopsis_thaliana
  Caenorhabditis_elegans
  Danio_rerio
  Drosophila_melanogaster
  Homo_sapiens
  Mus_musculus
  Rattus_norvegicus
  Saccharomyces_cerevisiae
  Schizosaccharomyces_pombe

Additional files within the ``DAMO`` directory are currently these:

========================= ===================================================
File                      Contents
========================= ===================================================
``metadata.tsv``          Metadata corresponding to datasets
========================= ===================================================

Alongside the species directories, each collection directory contains the
following directories:

========================= ===================================================
Directory                 Contents
========================= ===================================================
``Browser_tracks``        Data formatted for use by genome browsers
``Bulk_downloads``        Per-species and cumulative data archives
``LOLA_databases``        Per-species enrichment-related databases
========================= ===================================================


Processing Considerations
-------------------------

For convenience, the actual location of this directory structure should be
referenced via a symbolic link. This potentially helps in recording details of
when the data was processed and establishing versions of the data. For
example:

::

  ln -s path-to-directory-structure tfbsinit_20200918

Here, the digits provide a date code of the form ``YYYYMMDD`` to indicate a
version.


Preparing Import-Ready Data and Metadata
----------------------------------------

The given ``Makefile`` attempts to process the directory contents and to
produce data and metadata in appropriate forms for Web publication. It can be
run as follows from the top level of this distribution:

.. code-block:: shell

   make INPUT_DATA=tfbsinit_20200918

Once processing is complete, data and metadata archives are deployed as
follows:

.. code-block:: shell

   make install

This form of invocation is equivalent to the following:

.. code-block:: shell

   make OUTPUT_DATA=/var/tmp/unibind-data install

The ``OUTPUT_DATA`` argument can be explicitly specified, and this is
recommended if the data is to be copied to a different machine for deployment.

Once processing is complete, data and metadata archives will be written to the
indicated (or default) directory with names derived from the original archive.
For example:

::

  tfbs_20200918.tar.gz
  tfbs_metadata_20200918.tar.gz

These files should be understood by the UniBind deployment scripts, with the
former being used to populate the static data served by the Web application,
and the latter being used to populate the database.

Some other archives are also produced:

::

  bulk_Permissive_20200918.tar
  bulk_Robust_20200918.tar
  LOLA_databases_Permissive_20200918.tar.gz
  LOLA_databases_Robust_20200918.tar.gz
  UniBind_hubs_Permissive_20200918.tar.gz
  UniBind_hubs_Robust_20200918.tar.gz


Processing Details
------------------

Information about the processing done by this distribution can be found in the
dataflow_ document.

.. _dataflow: docs/Dataflow.rst


Copyright and Licensing Information
-----------------------------------

Source code originating in this project is licensed under the terms of the GNU
General Public License version 3 or later (GPLv3 or later). Original content
is licensed under the Creative Commons Attribution Share Alike 4.0
International (CC-BY-SA 4.0) licensing terms.

See the ``.reuse/dep5`` file and the accompanying ``LICENSES`` directory in
this software's source code distribution for complete copyright and licensing
information, including the licensing details of bundled code and content.
