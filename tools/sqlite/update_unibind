#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/../.."`

DATADIR_DEFAULT="$BASEDIR/work"
DATABASE_DEFAULT="/var/www/apps/unibind/UniBind.sqlite3"

DATADIR=${1:-$DATADIR_DEFAULT}
DATABASE=${2:-$DATABASE_DEFAULT}

if [ "$1" = '--help' ] || [ ! -e "$DATADIR" ] || [ ! -e "$DATABASE" ] ; then
    cat 1>&2 <<EOF
Usage: $0 <processed data directory> <UniBind database>

Update the given UniBind database with new data from the given data directory.
Without any parameters, the program is executed as if the following had been
specified:

$0 $DATADIR_DEFAULT $DATABASE_DEFAULT
EOF
    exit 1
fi

# Populate UniBind tables, replacing existing content.

INSERT="$THISDIR/import_table_data"

"$THISDIR/import_table_data" "$DATABASE" cell "$DATADIR/unibind_cell"
"$INSERT" "$DATABASE" factor "$DATADIR/unibind_factor"
"$INSERT" "$DATABASE" factor_data "$DATADIR/unibind_factor_data"
"$INSERT" "$DATABASE" factor_ref "$DATADIR/unibind_factor_ref"
"$INSERT" "$DATABASE" factor_sample "$DATADIR/unibind_factor_sample"

# Fix up missing profile references.

QUERY="update factor_data set profile_id = (select id from profiles where jaspar_id = factor_data.jaspar_id and jaspar_version = factor_data.jaspar_version) where profile_id is null or profile_id = ''"

sqlite3 "$DATABASE" "$QUERY"

"$THISDIR/missing_profiles" "$DATADIR" "$DATABASE"
"$THISDIR/append_table_data" "$DATABASE" profiles "$DATADIR/missing_profiles"

# Fix up null cell type references.

sqlite3 "$DATABASE" "update factor_sample set biological_condition = null where biological_condition = 'NULL'"
sqlite3 "$DATABASE" "update factor_sample set cell_id = null where cell_id = 0"
sqlite3 "$DATABASE" "update cell set cellosaurus_id = null where cellosaurus_id = 'NULL'"
sqlite3 "$DATABASE" "update cell set cell_ontology_id = null where cell_ontology_id = 'NULL'"
sqlite3 "$DATABASE" "update cell set exp_factor_ontology_id = null where exp_factor_ontology_id = 'NULL'"
sqlite3 "$DATABASE" "update cell set uberon_id = null where uberon_id = 'NULL'"
sqlite3 "$DATABASE" "update cell set source = null where source = 'NULL'"
sqlite3 "$DATABASE" "update cell set source_id = null where source_id = 'NULL'"
sqlite3 "$DATABASE" "update cell set cell_type_id = null where cell_type_id = 'NULL'"
sqlite3 "$DATABASE" "update cell set brenda_id = null where brenda_id = 'NULL'"
