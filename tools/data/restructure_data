#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/../.."`

INPUT_DATA=$1
TFBSDIR=${2:-"$BASEDIR/tfbs"}

OUTPUT_DATA="$TFBSDIR/macs/DAMO"

ensure_path()
{
    if [ -h "$1" ] ; then
        rm "$1"
    fi

    if [ ! -e "$1" ] ; then
        mkdir -p "$1"
    fi
}

rename()
{
    echo "$1" | tr ' ' '_'
}

# Ensure absolute paths for link targets.

INPUT_DATA=`realpath "$INPUT_DATA"`

# Create the directory hierarchy for restructured data.

if [ ! -e "$OUTPUT_DATA" ] ; then
    mkdir -p "$OUTPUT_DATA"
fi

# Read the folder information from standard input, obtaining details of the
# original and renamed folders.

TAB=`printf '\t'`
IFS="$TAB"

while read -r UBID FOLDER TF_NAME SPECIES MATRIX_ID MATRIX_VERSION TF_NAME_REWRITTEN ; do
    SPECIES_RENAMED=`rename "$SPECIES"`
    OLDPATH="$INPUT_DATA/DAMO/$SPECIES_RENAMED/$UBID"

    if [ ! -e "$OLDPATH" ] ; then
        echo "Missing folder: $OLDPATH" 1>&2
        continue
    fi

    NEWPATH="$OUTPUT_DATA/$FOLDER"
    ensure_path "$NEWPATH"

    PROFILE="${MATRIX_ID}.${MATRIX_VERSION}"

    # Current filename assumptions:
    # <profile>.damo.pwm

    OLDPWM="${OLDPATH}/${PROFILE}.damo.pwm"
    NEWPWM="${NEWPATH}/${FOLDER}.${PROFILE}.damo.pwm"

    ln -s "$OLDPWM" "$NEWPWM"

    # MACS_<tfname>_<profile>.damo.pwm*

    PREFIX="MACS_${TF_NAME}_${PROFILE}.damo.pwm"

    OLDIMAGE="${OLDPATH}/${PREFIX}_norescan.ez.png"
    NEWIMAGE="${NEWPATH}/${FOLDER}.${PROFILE}.damo.png"

    ln -s "$OLDIMAGE" "$NEWIMAGE"

    OLDBED="${OLDPATH}/${PREFIX}_rescan.score.ez.score_best.tsv.tfbs.bed"
    NEWBED="${NEWPATH}/${FOLDER}.${PROFILE}.damo.bed"

    ln -s "$OLDBED" "$NEWBED"

    OLDFASTA="${OLDPATH}/${PREFIX}_rescan.score.ez.score_best.tsv.tfbs.fa"
    NEWFASTA="${NEWPATH}/${FOLDER}.${PROFILE}.damo.fa"

    ln -s "$OLDFASTA" "$NEWFASTA"
done
