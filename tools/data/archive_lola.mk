# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Rules for archiving enrichment data from directories containing database
# files. Here, a template describes the rule to make each archive from each
# source directory. This template must be instantiated for each collection.

define lola_collection_template =
INPUT_LOLA_$(1) = $(INPUT_DATA)/$(1)_collection/LOLA_dbs
ARCHIVE_LOLA_$(1) = $(shell $(DATATOOLS)/archive_filename $(INPUT_DATA) $(1) LOLA_databases $(WORK))

$$(ARCHIVE_LOLA_$(1)): $$(INPUT_LOLA_$(1))
	$(TOOLS)/tar_create $$(ARCHIVE_LOLA_$(1)) $$(INPUT_LOLA_$(1))
endef
