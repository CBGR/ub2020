#!/bin/sh

# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Link CRMs into the bulk directory staging area and make links using
# appropriate naming to the directories containing aggregated data.

INPUT_DATA=$1
BULK_WORK_DIR=$2

if [ ! -e "$INPUT_DATA" ] || [ ! "$BULK_WORK_DIR" ] ; then
    cat 1>&2 <<EOF
Need a data directory containing collection data and an output directory.
EOF
    exit 1
fi

if [ ! -e "$BULK_WORK_DIR" ] ; then
    mkdir -p "$BULK_WORK_DIR"
fi

# Ensure absolute paths for link targets.

INPUT_DATA=`realpath "$INPUT_DATA"`

# Use this model in the file naming.

MODEL=damo

# Process each species within a collection.

for SPECIES_DIR in "$INPUT_DATA/"*"_collection"/* ; do

    # Obtain species directory names.

    SPECIES_NAME=`basename "$SPECIES_DIR"`
    PARENT=`dirname "$SPECIES_DIR"`
    COLLECTION_NAME=`basename "$PARENT"`

    # Store files in the corresponding location within the archiving directory.

    OUTPUT_DIR="$BULK_WORK_DIR/$COLLECTION_NAME/$SPECIES_NAME"

    # Find PWM archives in the bulk data bearing the genome name. These only
    # seem to reliably exist in the Bulk_downloads/Per_species section of the
    # collection data.

    for PWMS in "$SPECIES_DIR/../Bulk_downloads/Per_species/$SPECIES_NAME/"*"_PWMs.tar.gz" ; do

        # Handle the absence of any files.

        if [ ! -e "$PWMS" ] ; then
            continue
        fi

        BN=`basename "$PWMS"`
        GENOME=`echo "$BN" | sed 's/_PWMs.tar.gz$//'`

        # Link any companion CRMs and compressed TFBS files into the output directory.

        DN=`dirname "$PWMS"`
        DN=`realpath "$DN"`

        for CRMS in "$DN/"*"_CRMs.bed" ; do
            BN=`basename "$CRMS"`

            if [ -e "$CRMS" ] && [ ! -e "$OUTPUT_DIR/$BN" ] ; then
                ln -s "$CRMS" "$OUTPUT_DIR/$BN"
            fi
        done

        for TFBS in "$DN/"*"_compressed_TFBSs.bed.gz" ; do
            BN=`basename "$TFBS"`

            if [ -e "$TFBS" ] && [ ! -e "$OUTPUT_DIR/$BN" ] ; then
                ln -s "$TFBS" "$OUTPUT_DIR/$BN"
            fi
        done

        # Link to existing directories using names containing the model and
        # genome details.

        for FILENAME in "$OUTPUT_DIR/"* ; do
            if [ -d "$FILENAME" ] && [ ! -h "$FILENAME" ] ; then
                BN=`basename "$FILENAME"`

                # Link only if the link does not exist. Otherwise, the link gets
                # created inside the directory referring to itself.

                if [ ! -e "$OUTPUT_DIR/${MODEL}_${GENOME}_${BN}" ] ; then
                    ln -s "$BN" "$OUTPUT_DIR/${MODEL}_${GENOME}_${BN}"
                fi
            fi
        done

        # Process only one BED file per species directory.

        break
    done
done

# vim: tabstop=4 expandtab shiftwidth=4
