#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Output a file containing records of the following form:
# <folder> <TF name> <identifier> <matrix identifier> <matrix version>

INPUT_DATA=$1
CALLER=${2:-MACS}
TAB=`printf '\t'`

for PEAKS in `find -L "$INPUT_DATA" -name '*_peaks'` ; do
    DIRNAME=`dirname "$PEAKS"`
    FOLDER=`basename "$DIRNAME"`
    IDENTIFIER=${FOLDER%%_*}

    # Obtain TF name details using _peaks filenames.

    CALLER_TF_PEAKS=`basename "$PEAKS"`
    CALLER_TF=${CALLER_TF_PEAKS%_peaks}
    TF=${CALLER_TF#${CALLER}_}

    # Obtain matrix details using .damo.pwm filenames.

    for PWM in "$DIRNAME/"*".damo.pwm" ; do
        PWMNAME=`basename "$PWM" .damo.pwm`
        PROFILE_BASE=${PWMNAME%%.*}
        PROFILE_VERSION=${PWMNAME#*.}
        echo "${FOLDER}${TAB}${TF}${TAB}${IDENTIFIER}${TAB}${PROFILE_BASE}${TAB}${PROFILE_VERSION}"
    done
done
