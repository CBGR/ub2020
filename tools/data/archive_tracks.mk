# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Rules for archiving browser track data from directories containing database
# files. Here, a template describes the rule to make each archive from each
# source directory. This template must be instantiated for each collection.

define tracks_collection_template =
INPUT_TRACKS_$(1) = $(INPUT_DATA)/$(1)_collection/Browser_tracks
ARCHIVE_TRACKS_$(1) = $(shell $(DATATOOLS)/archive_filename $(INPUT_DATA) $(1) UniBind_hubs $(WORK))

$$(ARCHIVE_TRACKS_$(1)): $$(INPUT_TRACKS_$(1))
	$(TOOLS)/tar_create $$(ARCHIVE_TRACKS_$(1)) $$(INPUT_TRACKS_$(1))
endef
