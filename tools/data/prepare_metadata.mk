# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Process input data and metadata, producing metadata for further processing and
# refinement.

# Note that this Makefile should be invoked from the top level of the repository
# or distribution, not from its own directory.

BASEDIR = $(CURDIR)
TOOLS = $(BASEDIR)/tools
DATATOOLS = $(TOOLS)/data



# Input data and metadata.

ifndef INPUT_DATA
$(error INPUT_DATA must be defined)
endif

INPUT_METADATA		?= $(shell ls -1t $(INPUT_DATA)/metadata* | head -n 1)



# Metadata processing artefacts.

WORK			?= $(BASEDIR)/work
AGGREGATED		?= $(WORK)/aggregated

# Folder details and related metadata.

FOLDERS			= $(AGGREGATED)/folders
MAPPING			= $(AGGREGATED)/mapping
NAME_MAPPING		= $(AGGREGATED)/name_mapping
FOLDER_MAPPING		= $(AGGREGATED)/folder_mapping
FOLDER_MEMBERSHIP	= $(AGGREGATED)/folder_membership
FOLDER_METADATA		= $(AGGREGATED)/folder_metadata
FOLDER_METADATA_UPDATED	= $(AGGREGATED)/folder_metadata_updated
FOLDER_MISSING		= $(AGGREGATED)/folder_missing

# Extracted and manipulated metadata.

EXPERIMENT_PROFILES	= $(AGGREGATED)/experiment_profiles
MATRIX_SCORES		= $(AGGREGATED)/matrix_scores
OUTPUT_METADATA		= $(AGGREGATED)/metadata.txt
PEAK_TOTALS		= $(AGGREGATED)/peak_totals
TFBS_TOTALS		= $(AGGREGATED)/tfbs_totals

# Completion indicator.

COMPLETED		= $(AGGREGATED)/completed

# Outputs for further processing.

OUTPUTS	= $(EXPERIMENT_PROFILES) $(FOLDERS) $(FOLDER_MEMBERSHIP) $(FOLDER_MISSING) \
	  $(MATRIX_SCORES) $(OUTPUT_METADATA) $(PEAK_TOTALS) $(TFBS_TOTALS)

# All outputs for deletion purposes.

ALL_OUTPUTS = $(COMPLETED) $(OUTPUTS) $(MAPPING) $(NAME_MAPPING) $(FOLDER_MAPPING) \
	      $(FOLDER_METADATA) $(FOLDER_METADATA_UPDATED)



# Processing workflow.

.PHONY : all clean

all: $(COMPLETED)

clean:
	-$(RM) $(ALL_OUTPUTS)
	-rmdir $(AGGREGATED)



# Record processing completion.

$(COMPLETED): $(OUTPUTS)
	touch $(COMPLETED)

# Make the working directory for metadata.

$(ALL_OUTPUTS): | $(AGGREGATED)

$(AGGREGATED):
	mkdir -p $(AGGREGATED)



# Produce metadata for import by stripping off the headings.

$(OUTPUT_METADATA): $(INPUT_METADATA)
	tail -n +2 $(INPUT_METADATA) > $(OUTPUT_METADATA)



# Metadata rewriting.

# Obtain revised folder details.

$(FOLDERS): $(FOLDER_MAPPING)
	$(DATATOOLS)/rewrite_folder_values < $(FOLDER_MAPPING) > $(FOLDERS)

# Combine folder and TF mapping details to find data.

$(FOLDER_MAPPING): $(FOLDER_METADATA_UPDATED) $(NAME_MAPPING)
	$(DATATOOLS)/get_folder_mapping $(FOLDER_METADATA_UPDATED) $(NAME_MAPPING) > $(FOLDER_MAPPING)

# Obtain missing folder details.

$(FOLDER_METADATA_UPDATED): $(FOLDER_METADATA)
	$(DATATOOLS)/check_folders $(INPUT_DATA) < $(FOLDER_METADATA) > $(FOLDER_METADATA_UPDATED) 2> $(FOLDER_MISSING)

# Obtain concise details of folders, together with species and experiment
# details.

$(FOLDER_METADATA): $(OUTPUT_METADATA)
	$(DATATOOLS)/get_folders < $(OUTPUT_METADATA) > $(FOLDER_METADATA)

# Use the supplied mapping to obtain profile information.

$(EXPERIMENT_PROFILES): $(MAPPING)
	$(DATATOOLS)/make_profiles < $(MAPPING) > $(EXPERIMENT_PROFILES)

# Use the supplied mapping to obtain name information.

$(NAME_MAPPING): $(MAPPING)
	$(DATATOOLS)/make_names < $(MAPPING) > $(NAME_MAPPING)



# Aggregation of metadata from the data folders.

# Obtain the TF name mapping for combination with folder details.

$(MAPPING): $(INPUT_DATA)
	$(DATATOOLS)/get_mapping $(INPUT_DATA) > $(MAPPING)

# Obtain scores for the data.

$(MATRIX_SCORES): $(INPUT_DATA)
	$(DATATOOLS)/aggregate_scores $(INPUT_DATA) > $(MATRIX_SCORES)

# Obtain peak totals for the data.

$(PEAK_TOTALS): $(INPUT_DATA)
	$(DATATOOLS)/count_peak_records $(INPUT_DATA) > $(PEAK_TOTALS)

# Obtain TFBS totals for the data.

$(TFBS_TOTALS): $(INPUT_DATA)
	$(DATATOOLS)/count_tfbs_records $(INPUT_DATA) > $(TFBS_TOTALS)

# Obtain folder membership details, indicating the members of each collection.

$(FOLDER_MEMBERSHIP): $(INPUT_DATA)
	$(DATATOOLS)/get_folder_membership $(INPUT_DATA) > $(FOLDER_MEMBERSHIP)
