# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Rules for archiving bulk download data from directories containing data files.
# Here, a template describes the rule to make each archive from each source
# directory. This template must be instantiated for each collection.

define bulk_collection_template =
ARCHIVE_BULK_$(1) = $(shell $(DATATOOLS)/archive_filename $(INPUT_DATA) $(1) bulk $(WORK) --uncompressed)
WORK_BULK_$(1) = $(BULKDATA_ARCHIVED)/$(1)_collection

$$(ARCHIVE_BULK_$(1)): $$(WORK_BULK_$(1))
	$(TOOLS)/tar_create $$(ARCHIVE_BULK_$(1)) $$(WORK_BULK_$(1))
endef
