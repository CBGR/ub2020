Dataflow
========

The UniBind Web application has traditionally employed a particular
hierarchical structure for the data provided by the application, together with
a particular database schema for the corresponding metadata. This software is
intended to package the data provided by the ChIP-eat software when itself
invoked on input data. Ultimately, the intention is to integrate this software
with ChIP-eat to produce packaged data suitable for use directly with the
UniBind Web application.

.. contents::

Activities
----------

The following diagram summarises the different activities involved in
transforming the input data provided by ChIP-eat into the output data required
by UniBind.

.. image:: Dataflow.svg
   :width: 100%

The details of the different activities are described below.

Metadata Aggregation
''''''''''''''''''''

Some essential metadata does not appear to be provided by the ``metadata.tsv``
file. Consequently, such metadata is acquired by examining the datasets found
in the per-species dataset directories:

- Peak records (``_peaks``) provide peak total information
- PWM file metadata (``.pwm``) provides TF name and matrix profile metadata
- Score records (``.score.thr``) provide score information
- TFBS records (``.tsv.tfbs.bed``) provide TFBS total information

TF name details are incorporated into folder metadata processing.

Folder Metadata
'''''''''''''''

The ``metadata.tsv`` file provides several columns of metadata related to
datasets, cell types and experiments, referencing dataset folders using the
``UBID`` column. However, the folder naming employed by the folders and this
column is not consistent with the traditional naming employed by the UniBind
Web application.

Consequently, folder metadata is extracted from ``metadata.tsv`` and combined
with TF name details provided by the metadata aggregation process, the
presence of the folders is checked, and the details of those present are
augmented with renamed or rewritten values for the following fields:

- Cell line/type
- TF name
- Treatment

Collection Membership
'''''''''''''''''''''

The membership of datasets in various collections does not appear to be
recorded in any concise form, with membership being implied by the presence of
dataset folders in the ``fasta_per_TFBS`` subdirectory of each species
directory within each collection.

Consequently, folder membership is deduced by aggregating folder names within
each ``fasta_per_TFBS`` subdirectory, noting the collection within which each
subdirectory resides.

Bulk, Enrichment and Track Data
'''''''''''''''''''''''''''''''

Each collection provides bulk download, enrichment database and genome browser
(track) data. The enrichment and track data can generally be packaged as is.
However, bulk download data needs to be "restructured" (as described in the
section below) and may need some additional modifications as described below.

The ``tools/data/copy_bulk_data`` script, invoked by the ``Makefile``,
introduces a prediction model prefix to most download filenames.  Although
this prefix, ``damo``, is effectively superfluous for UniBind data where only
one prediction model has been applied, older versions of UniBind data may
involve several prediction models. In order to be able to present downloads in
a consistent way in the Web application, model prefixes have been introduced
to allow the convenient generation of download filenames.

The genome declarations in the ``genomes.txt`` file, found within each
collection of genome browser data, must ensure that declarations are
appropriate for the UCSC and Ensembl genome browsers. Genomes ``hg38``,
``mm10``, ``ce11``, ``dm6``, ``danRer11`` and ``rn6`` are understood by both
browsers. The Schizosaccharomyces pombe genome ``ASM294v2`` needs to be
defined for both UCSC and Ensembl.

A declaration of Saccharomyces cerevisiae genome ``sacCer3`` must be present
for UCSC, whereas ``R64-1-1`` must be present for Ensembl. Effectively, these
are treated as aliases for each other.

Meanwhile, a declaration of Arabidopsis thaliana genome ``TAIR10`` must be
present for Ensembl, whereas the assembly needs to be defined for UCSC. This
declaration and definition are combined: presumably, Ensembl can ignore the
definition if necessary.

The bulk download, enrichment and track data are packaged using Makefile rules
that are provided by the following template files:

- ``tools/data/archive.bulk.mk``
- ``tools/data/archive.lola.mk``
- ``tools/data/archive.tracks.mk``

These templates are "instantiated", once per collection, in order to provide
rules that describe the production of each output archive.

Metadata Preparation and Export
'''''''''''''''''''''''''''''''

With metadata aggregated and augmented with membership and folder naming
information, the metadata can be arranged in a form suitable for further
UniBind usage. It may then be exported in tabular form, packaged, and
deployed.

In this process, the metadata is imported into a database system initialised
with a schema defined by the ``sql/init_experiments_preparation.sql`` file
using the ``tools/sqlite/prepare_metadata`` script, which invokes the
``sql/init_experiments_preparation_data_sqlite.sql`` script. This latter
script performs a number of tasks:

- Input data normalisation: defining consistent and appropriate values
- Species correction: some records have previously had incorrect values
- Identifier selection: choosing external or GTRD experiment identifiers
- Folder name construction: rewriting folder names for UniBind purposes

The metadata is then organised according to the entities in the UniBind
schema:

- Cell line/type
- Dataset (factor data)
- Dataset membership (factor collection)
- Factor
- Reference (factor reference)
- Sample (factor sample)
- Species membership (species collection)

Additional output indicating renamed files and folders is employed by the data
structuring activity.

Data Restructuring
''''''''''''''''''

Since the names by which datasets (folders) are known by UniBind may differ
from the names employed in the input data, it is not appropriate to merely
package the supplied input data. Instead, the input data must be restructured
and renamed to meet the expectations of the UniBind application.

The ``tools/data/restructure_data`` script takes the ``renamed_files`` output
from metadata preparation and transfers input data to an output data
hierarchy, employing appropriate file and directory naming, so that the
completed collection of files can be packaged and subsequently deployed.

Meanwhile, the ``tools/data/restructure_bulk_data`` script takes the
``renamed_collection_files`` output derived from ``renamed_files`` and the
details of the profile collections, transferring input data to an output data
hierarchy, employing the same naming conventions as the individual data
records. Various arrangements of files are archived for eventual deployment,
and these archives will ultimately be combined into a single archive for
deployment.
